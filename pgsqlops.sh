#!/usr/bin/env bash
# PGSQL ops
# Author: Stephane Paillet
# Date: 2023-04-26

# Vars
scriptpath=$(pwd)
deployuser='root'
param=$2
limit=$3

function DISPLAY_USAGE {
echo "Usage : $(basename $0) [-h] action param limit
Quickly and ugly script :)
where:
   -h  	   : show help command
   action : action to launch
   param  : dbname
   limit  : host where launch action"
}

function DBCREATE {
  ansible-playbook ${scriptpath}/pgsql-ops.yml -e "ansible_user='${deployuser}' pgsql_op=dbcreate pgsql_dbname='${param}'" --ask-vault-password --limit '${limit}'
}

function DBDROP {
  ansible-playbook ${scriptpath}/pgsql-ops.yml -e "ansible_user='${deployuser}' pgsql_op=dbdrop pgsql_dbname='${param}'" --ask-vault-password --limit '${limit}'
}

# Check Usage
[[ $# -ne 2 ]] && DISPLAY_USAGE && exit 1
[[ "$1" == "-h" ]] && DISPLAY_USAGE && exit 0

# Main
if [ $1 == dbcreate ]; then
  DBCREATE
elif [ $1 == dbdrop ]; then
  DBDROP
fi

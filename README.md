# PGSQL ops

"Quickly, ugly and dirty" scripts to perform ops on an PostgreSQL instance.

Use pgsql-ops Ansible rôle.

## Examples

Create a database named "test" on "db01" host :

```
bash ./pgsqlops.sh dbcreate test db01
```

Drop a database named "test" on "db01" host :

```
bash ./pgsqlops.sh dbdrop test db01
```